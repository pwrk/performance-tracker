#!/bin/bash
set -e

BACKPROC="0"
DB="backend/.tmp/data.db"
ENV="backend/.env"

end() {
    if [ "$BACKPROC" != "0" ]; then
        kill $BACKPROC
    fi
    if [ -f "${DB}.bak" ]; then
        mv ${DB}.bak $DB
        mv ${ENV}.bak $ENV
    fi
    if [ $1 != 0 ]; then
        echo "Test failed - see client.log and server.log"
        cat server.log
        cat client.log
    fi
    echo -n "STOP Strapi Backend. " && ( kill -- -$$ || true )
    exit $1
}
trap 'end $?' EXIT

BACKPORT=1337

if [ -f "$DB" ]; then
  mv $DB  ${DB}.bak
  touch $ENV && mv $ENV ${ENV}.bak
fi

echo "SEND_BEACON_TARGET=http://127.0.0.1:$BACKPORT/" > packages/tracker/.env
echo "MAX_VISIT_LENGTH=1800000" >> packages/tracker/.env
echo "CLASS_APPLICATION_LINK=pwrk-application-link" >> packages/tracker/.env
echo "DISABLE_TRACKING=disable-tracking" >> packages/tracker/.env

yarn build
cp packages/tracker/dist/tracker.js backend/public/

strapicmd="strapi admin:create-user --firstname=test --email=test@test.test --password=Testing1234"
echo "APP_KEYS=TestKey" > backend/.env
echo "API_TOKEN_SALT=TestSalt" >> backend/.env
echo "ADMIN_JWT_SECRET=TestJwt" >> backend/.env
echo "TRANSFER_TOKEN_SALT=TestJwtTransfer" >> backend/.env
echo "STRAPI_PUBLIC_URL=http://127.0.0.1:1337/" >> backend/.env

yarn --cwd backend
yarn --cwd backend $strapicmd

LOG_IP_ON_PAGEVIEWS_MAX=2 yarn --cwd backend develop > server.log 2>&1 &
BACKPROC=$!
echo "STARTED Strapi Backend. Proc ID: $BACKPROC"

node --trace-uncaught --trace-warnings test.js > client.log 2>&1

echo "TEST OK"


