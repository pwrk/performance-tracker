# Performance tracking Backend

The performance tracking data is stored in a Strapi. 

By default Strapi uses a SQLite database.

## Development

A development envelope is implemented by:

```
git clone git@gitlab.com:pwrk/performance-tracker.git
cd performance-tracker
yarn && yarn bootstrap
cd backend
yarn develop
```

## Testing

A functional test is performed per:

`sh run_test.sh`

he moves the locale `.tmp/data.db` to `.tmp/data.db.bak`, starts a new
strapi instance, creates an admin user and starts the `test.js`.

## production 

the development environment can be started in production mode:

Production mode runs a MariaDB >=10.

```
NODE_ENV=production yarn build
NODE_ENV=production yarn start
```

The following configuration can be configured via `.env`.



```
# IP, on which Strapi listens. 0.0.0.0 listens on all IPs
HOST=0.0.0.0

# PORT on which Strapi listens
PORT=1337

# different parameters that Strapi needs to start.
APP_KEYS=pleaseChangeMe
JWT_SECRET=pleaseChangeMe
API_TOKEN_SALT=pleaseChangeMe
ADMIN_JWT_SECRET=pleaseChangeMe
TRANSFER_TOKEN_SALT=pleaseChangeMe

#
STRAPI_PUBLIC_URL=http://localhost:1337/

# Number of pageviews that can be attributed to a visit before a
# user IP is stored.
LOG_IP_ON_PAGEVIEWS_MAX=1

# Maria DB in Production Mode `NODE_ENV=production yarn develop`
DATABASE_HOST=127.0.0.1
DATABASE_PORT=3306
DATABASE_NAME=performance_tracker
DATABASE_USERNAME=****
DATABASE_PASSWORD=****
```

