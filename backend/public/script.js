var _pt = _pt || {};
(function() {
  var u=window.location.hostname === 'pwrk.gitlab.io' ? "//count.mediaintown.de/" : '/';
  _pt['PAGE'] = 1234567;
  _pt['CHANNEL'] = 2345;
  _pt['CONTEXT'] = window.localStorage.getItem('CONTEXT') ?? 'jobAd'; 
  var d=document, g=d.createElement("script"), s=d.getElementsByTagName("script")[0]; g.type="text/javascript";
  g.defer=true; g.async=true; g.src=u+"tracker.js"; s.parentNode.insertBefore(g,s);
})();
