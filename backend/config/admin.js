module.exports = ({ env }) => ({
  auth: {
    secret: env("ADMIN_JWT_SECRET", "f06f99cefce37bH08d6a648af94e3e8b"),
  },
  apiToken: {
    salt: env("API_TOKEN_SALT", "ELI1QjBVNyYoSFp2nNgdqdjgRM0c7X4y"),
  },
  transfer: {
    token: {
      salt: env("TRANSFER_TOKEN_SALT", "ELI1QjBVNyMoSFp2nNgdqdjgRM0c7X5y"),
    },
  },
});
