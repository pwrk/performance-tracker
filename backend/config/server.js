const cronTasks = require("./cron-tasks");

module.exports = ({ env }) => ({
  host: env("HOST", "0.0.0.0"),
  port: env.int("PORT", 1337),
  app: {
    keys: env.array("APP_KEYS", ["testCey1", "testCey2"]),
  },
  proxy: true,
  url: env("STRAPI_PUBLIC_URL", "https://pt.jobsintown.de"),
  cron: {
    enabled: true,
    tasks: cronTasks,
  },
});
