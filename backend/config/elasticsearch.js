
module.exports = ({ env }) => ({
  connection: {
    // https://www.elastic.co/guide/en/elasticsearch/client/javascript-api/current/auth-reference.html
    node: env('ELASTICSEARCH_HOST', 'http://127.0.0.1:9200'),
    auth: {
      username: env('ELASTICSEARCH_USERNAME'),
      password: env('ELASTICSEARCH_PASSWORD'),
    },
  },
  description: 'Test',
  settings: {
    importLimit: 3000,
    validStatus: [200, 201],
    validMethod: ['PUT', 'POST', 'DELETE'],
    fillByResponse: false,
    index_prefix: '',
    index_postfix: '',
    removeExistIndexForMigration: true,
  },
  models: [
  {
    "model": "pageview",
    "index": "pageview",
    "plugin": null,
    "enabled": true,
    "migration": true,
    "pk": "id",
    "relations": [],
    "conditions": {},
    "fillByResponse": true,
    "supportAdminPanel": true,
    "urls": []
  },
  {
    "model": "visit",
    "index": "visit",
    "plugin": null,
    "enabled": true,
    "migration": true,
    "pk": "id",
    "relations": [],
    "conditions": {},
    "fillByResponse": true,
    "supportAdminPanel": true,
    "urls": []
  },
  {
    "model": "visitor",
    "index": "visitor",
    "plugin": null,
    "enabled": true,
    "migration": true,
    "pk": "id",
    "relations": [],
    "conditions": {},
    "fillByResponse": true,
    "supportAdminPanel": true,
    "urls": []
  }
]
});