
module.exports = ({ env }) => ({
  settings: {
    useElastic: env('ELASTICSEARCH_ENABLED', false),
    clicksWebhookUrl: env('CLICKS_WEBHOOK_URL', false),
    enableWebhookFor: env.array('ENABLE_WEBHOOK_FOR', [2345])
  },
});