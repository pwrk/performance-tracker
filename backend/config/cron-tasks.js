// path: ./config/cron-tasks.js
//
//  *    *    *    *    *    *
//  ┬    ┬    ┬    ┬    ┬    ┬
//  │    │    │    │    │    |
//  │    │    │    │    │    └ day of week (0 - 7) (0 or 7 is Sun)
//  │    │    │    │    └───── month (1 - 12)
//  │    │    │    └────────── day of month (1 - 31)
//  │    │    └─────────────── hour (0 - 23)
//  │    └──────────────────── minute (0 - 59)
//  └───────────────────────── second (0 - 59, OPTIONAL)



module.exports = {
  /**
   * Simple example.
   * Every monday at 1am.
   */

  '* * * * *': ({ strapi }) => {
    // importJobPortalLink();
  },
};

async function importJobPortalLink() {
  try {
    const data = await strapi.service('api::portal-link.portal-link').importPortalLinks()
    var latestTimestamp = 0
    var keys = []
    var created_at;
    for (const row of data) { 
      created_at = await strapi.service('api::portal-link.portal-link').getExternalPortalLink(row)
      latestTimestamp = Math.max(new Date(latestTimestamp),new Date(created_at))
    }
    if (latestTimestamp > 0) {
      strapi.query("api::setting.setting").update({ where: { id: 1 }, data: { lastImport: new Date(latestTimestamp) } });
    }else{
      console.log("cannot update lastImport", latestTimestamp)
    }
  } catch (error) {
    console.error(error);
  }
}
