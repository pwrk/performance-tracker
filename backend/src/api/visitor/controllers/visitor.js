'use strict';

/**
 *  visitor controller
 */

const { createCoreController } = require('@strapi/strapi').factories;

module.exports = createCoreController('api::visitor.visitor', ({ strapi }) => ({
    async find(ctx) {
        try {
            let payload = ctx.query;
            let visitors = await getAllVisitors();
            let reportDateWise = [{ date: new Date(), hits: 0 }]
            let reportCannelWise = []
            for (let _visitor of visitors) {
                let visits = await getAllVisits({ visitor: _visitor.visits });
                _visitor['visits'] = visits
                _visitor['VisitCountPerChannel'] = visits && Array.isArray(visits) ? visits.length : 0;
                let _total = 0
                for (let _visit of visits) {
                    let pageview = await getAllPageviews({ visit: _visit.pagevies });
                    _visit['pageview'] = pageview
                    _visit['pageviewCountPerVisit'] = pageview && Array.isArray(pageview) ? pageview.length : 0;
                    _total = _total + _visit['pageviewCountPerVisit']

                }
                reportCannelWise.push({ channel: _visitor.agent, hits: _total })
            }
            let pageviews = await getAllPageviews();
            pageviews = pageviews.map(x => {
                if (x.visibility) x['date'] = new Date(x.visibility).toDateString()
                if (x.visibilityChange) x['hits'] = x.visibilityChange
                return x;
            })
            reportDateWise = groupDateWise(pageviews)
            return {
                success: {
                    allResult: visitors, // Just for testing
                    reportDateWise,
                    reportCannelWise
                }
            }
        } catch (e) {
            return {
                error: {
                    status: 5000,
                    name: "internal_error",
                    message: " " + e,
                },
            };
        }
    },
}));

/**
 * INFO: Group results
 * @param {Array} items 
 * @param {String} key
 * @returns All pageview array
 */
const groupBy = (items, key) => items.reduce(
    (result, item) => ({ ...result, [item[key]]: [...(result[item[key]] || []), item] }),
    {}
);

/**
 * INFO: Group results by date
 * @param {Array} items 
 * @param {String} key
 * @returns All pageview array
 */
const groupDateWise = (items) => {
    Array.from(items.reduce((m, { date, hits }) => m.set(date, (m.get(date) || 0) + hits), new Map), ([date, hits]) => ({ date, hits }));
}

/**
 * INFO: Get all pageview
 * @param {Object} payload 
 * @returns All pageview array
 */
const getAllPageviews = async (payload = null) => {
    let pageview = payload ? await strapi.service("api::pageview.pageview").find(payload) : await strapi.service("api::pageview.pageview").find();
    return pageview.results
}

/**
 * INFO: Get all visits
 * @param {Object} payload 
 * @returns All visits array
 */
const getAllVisits = async (payload = null) => {
    let visit = payload ? await strapi.service("api::visit.visit").find(payload) : await strapi.service("api::visit.visit").find();
    return visit.results
}

/**
 * INFO: Get all visitors
 * @param {Object} payload 
 * @returns All visitors array
 */
const getAllVisitors = async (payload = null) => {
    let visitors = payload ? await strapi.service("api::visitor.visitor").find(payload) : await strapi.service("api::visitor.visitor").find();
    return visitors.results
}
