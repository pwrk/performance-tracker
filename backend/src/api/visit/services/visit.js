'use strict';

/**
 * visit service.
 */

const { createCoreService } = require('@strapi/strapi').factories;

module.exports = createCoreService('api::visit.visit', ({ strapi }) => ({
    /**
     * INFO: Get visit by Id
     * @param {*} filters 
     * @returns Matched visit
     */
    async visitById(id) {
        console.log("## visitById",id);
        const entries = await strapi.entityService.findOne('api::visit.visit', id);
        return entries
    },

    /**
     * INFO: Add visit
     * @param {*} visit 
     * @returns Added visit
     */
    async addVisit(visitObj) {
        let visit = await strapi.query('api::visit.visit').create(visitObj);
        return {
            success: {
                visit: visit
            }
        }
    }
}));

