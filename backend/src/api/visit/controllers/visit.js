'use strict';

/**
 *  visit controller
 */

const { createCoreController } = require('@strapi/strapi').factories;

module.exports = createCoreController('api::visit.visit', ({ strapi }) => ({
    async find(ctx) {
        try {
            console.log("Visit Controller", ctx);
            let payload = ctx.query;
            let visit = await getAllVisits();
            for(let item of visit) {
                let pageview = await getAllPageviews({ visit: item.id });
                item['pageview'] = pageview
            }
            return {
                success: {
                    visit: visit
                }
            }
        } catch (e) {
          return {
            error: {
              status: 5000,
              name: "internal_error",
              message: " " + e,
            },
          };
        }
      },
}));

/**
 * INFO: Get all pageview
 * @param {Object} payload 
 * @returns All pageview array
 */
 const getAllPageviews = async (payload = null) => {
    let pageview = payload ? await strapi.service("api::pageview.pageview").find(payload) : await strapi.service("api::pageview.pageview").find();
    return pageview.results
}

/**
 * INFO: Get all visits
 * @returns All visits array
 */
const getAllVisits = async () => {
    let visit = await strapi.service("api::visit.visit").find();
    return visit.results;
}