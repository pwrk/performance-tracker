// path: ./src/api/pageview/routes/01-pixel-tracker.js

module.exports = {
  routes: [
    { // Path defined with an URL parameter
      method: 'GET',
      path: '/pageview/:id/:channel/:company', 
      handler: 'pageview.count',
      config: {
        auth: false
      }
    },
    { // Path defined with an URL parameter
      method: 'GET',
      path: '/pageview/:id/:channel/:company/:title', 
      handler: 'pageview.count',
      config: {
        auth: false
      }
    },
  ]
}