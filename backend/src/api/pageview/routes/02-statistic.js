// path: ./src/api/pageview/routes/01-pixel-tracker.js

module.exports = {
  routes: [
    { // Path defined with an URL parameter
      method: 'GET',
      path: '/statistic/:id/date', 
      handler: 'pageview.statisticByDate',
      config: {
        auth: false
      }
    },
    { // Path defined with an URL parameter
      method: 'GET',
      path: '/statistic/:id/channel', 
      handler: 'pageview.statisticByChannel',
      config: {
        auth: false
      }
    },
  ]
}