'use strict';

/**
 * pageview router.
 */

const { createCoreRouter } = require('@strapi/strapi').factories;

module.exports = createCoreRouter('api::pageview.pageview');
