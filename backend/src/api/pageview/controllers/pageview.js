'use strict';
const { v4: uuid } = require('uuid');

/**
 *  pageview controller
 */

const { createCoreController } = require('@strapi/strapi').factories;

module.exports = createCoreController('api::pageview.pageview', ({ strapi }) => ({

    async statisticByDate(ctx) {
        const knex = strapi.db.connection;
        const job = ctx.params.id.replaceAll(/<\/?[^>]+(>|$)/gi, ""); 
        const sql = `SELECT date_format(created_at,"%Y-%m-%d") as date, count(1) as clicks, sum(page_reload -1) as reload, sum(duration) as sum_duration from pageview WHERE job='${job}' group by date order by created_at desc`; 
        try {
            let result = await knex.raw(sql).then((row) => row);
            ctx.body = result[0];
        } catch (error) {
            ctx.body = error;
        }
    },
    async statisticByChannel(ctx) {
        const knex = strapi.db.connection;
        const job = ctx.params.id.replaceAll(/<\/?[^>]+(>|$)/gi, ""); 
        const sql = `SELECT pv.channel, count(1) as clicks, sum(page_reload - 1) as reload , sum(duration) AS sum_duration, if(isnull(url),uri,url) AS url ` +
                    `FROM pageview AS pv LEFT JOIN portal_links AS pl on (pv.job=pl.job and pv.channel=pl.channel) ` +
                    `WHERE pv.job='${job}' group by pv.channel order by clicks desc`; 
 
        try {
            let result = await knex.raw(sql).then((row) => row);
            ctx.body = result[0];
        } catch (error) {
            ctx.body = error;
        }
    },
    // image tracking.
    async count(ctx) {
        
        const job = ctx.params.id.replaceAll(/<\/?[^>]+(>|$)/gi, ""); 
        const channel = ctx.params.channel.replaceAll(/<\/?[^>]+(>|$)/gi, "");
        const title = ctx.params.title?.replaceAll(/<\/?[^>]+(>|$)/gi, "") || null;
        const company = ctx.params.company.replaceAll(/<\/?[^>]+(>|$)/gi, "");
        
        const payload = {
            data: {
                uuidv4: uuid(),
                job: job,
                channel: channel,
                title: title,
                remote_host: ctx.ip,
                company: company,
                referrer: ctx.request.header.referer || null,
                uri: `https://anzeigen.jobsintown.de/job/${channel}/${job}.html` // make this configurable
            }
        }
        try {
            let pv = await strapi.query("api::pageview.pageview").create(payload);

            delete pv.emailClicked;
            delete pv.applyClicked;
            delete pv.contentCopied;
            delete pv.applyForm;
            delete pv.linkClicked;
            delete pv.remoteHost;
            delete pv.mouseTrail;
            delete pv.referrer;
            delete pv.title;
            delete pv.visibilityChange;
            delete pv.id;
            delete pv.duration;
            delete pv.pageReload;
            delete pv.firstLoadingTime;
            delete pv.viewHeight;
            delete pv.viewWidth;
            delete pv.scrollMax;
            delete pv.company;
            delete pv.job;
            delete pv.channel;
            delete pv.updatedAt;
            
            const buffer = Buffer.alloc(43)
            buffer.write('R0lGODlhAQABAIAAAAAAAAAAACH5BAEAAAAALAAAAAABAAEAAAICRAEAOw=', 'base64')
            ctx.type = 'image/gif';
            ctx.body = buffer;
            ctx.response.lastModified = new Date();
        } catch (err) {
            ctx.body = err;
        }
    },
    async create(ctx) {
        try {
            const ct = ctx.request.type;
            let bodyData = {};
            if (ct == "text/plain") {
                bodyData = ctx.request.body ? JSON.parse(ctx.request.body) : {};
            } else {
                bodyData = ctx.request.body ? JSON.parse(JSON.stringify(ctx.request.body)) : {};
            }
            const viewUuid = bodyData?.data?.pageview?.uuidv4 || uuid();
            const visitorUuid = bodyData?.data?.visitor?.uuidv4 || uuid();
            let result;
            let { _visit, _visitId } = await createVisit(strapi, bodyData, visitorUuid)
            if (_visitId) {
                let { pageview = {} } = bodyData?.data;
                const newPageview = createPageviewObject(pageview, _visitId);
                const checkPageview = await strapi.db.query('api::pageview.pageview').findOne({where: { uuidv4: viewUuid}, visit: _visitId});
                if (checkPageview && checkPageview.uuidv4) {
                    result = await strapi.service('api::pageview.pageview').edit(newPageview.data, _visitId, _visit, bodyData?.data?.visit.pageviews, ctx.ip);
                } else {
                    result = await createPageview(strapi, newPageview , bodyData?.data?.visit.pageviews, ctx.ip);
                }
            }
            return {
                success: {
                    pageview: result
                }
            }
        } catch (e) {
            console.debug(e);
            return {
                error: {
                    status: 5000,
                    name: "internal_error",
                    message: " " + e,
                },
            };
        }
    },
    async findOne(ctx) {
        const { LinkPageviewed, uuidv4 } = ctx.query;
        try {
            let isPageviewed = false;
            if (LinkPageviewed == true || LinkPageviewed == 'true') {
                isPageviewed = true;
            }
            const pageview = await strapi.db.query('api::pageview.pageview').findOne({where: { uuidv4: uuidv4 },});
            return {
                success: {
                    pageview: pageview
                }
            }
        } catch (e) {
            return {
                error: {
                    status: 5000,
                    name: "internal_error",
                    message: " " + e,
                },
            };
        }
    },
}))

/**
 * INFO: Create visit object
 * @param {Object} payload 
 * @returns visit
 */
const createVisitObject = (payload, uuidv4, visitorUuid) => {
    let newVisit = {
        data: {
            uuidv4: uuidv4 || uuid(),
            visitor: visitorUuid || null,
            channel: payload.channel || null,
            referrer: payload.referrer || null
        },
    };
    return newVisit;
}

/**
 * INFO: Create visitor object
 * @param {Object} payload 
 * @returns visitor
 */
const createVisitorObject = (payload, visitId) => {
    let newVisitor = {
        data: {
            uuidv4: payload.uuidv4 || uuid(),
            visits: null,
            agent: payload.agent || '',
            screenHeight: payload.screenHeight || null,
            screenWidth: payload.screenWidth || null,
            languages: payload.languages || null,
            language: payload.language || null,
            timezone: payload.timezone || null,
        },
    };
    return newVisitor;
}

/**
 * INFO: Create Pageviewviews object
 * @param {Object} payload 
 * @param {String} visitId 
 * @returns pageview
 */
const createPageviewObject = (payload, visitId) => {
    let newPageview = {
        data: {
            uuidv4: payload.uuidv4 || uuid(),
            visibilityChange: Number(payload.visibilityChange) || 1,
            scrollMax: Number(payload.scrollMax) || 0,
            pageReload: payload.pageReload || 0,
            visit: visitId,
            duration: payload.duration || 0,
            job: payload.job || '',
            uri: payload.uri || '',
            firstLoadingTime: payload.firstLoadingTime || 0,
            channel: payload.channel || '',
            company: payload.company || null,
            linkClicked: payload.linkClicked || null,
            applyClicked: payload.applyClicked || false,
            applyForm: payload.applyForm || false,
            emailClicked: payload.emailClicked || null,
            contentCopied: payload.contentCopied || 0, 
            title: payload.title || null,
            referrer: payload.referrer || null,
            remoteHost: payload.remoteHost || null,
            viewHeight: payload.viewHeight || null,
            viewWidth: payload.viewWidth || null
        },
    };
    return newPageview;
}


/**
 * INFO: Create visit
 * @param {Scope} strapi 
 * @param {Object} payload 
 * @param {String} visitId 
 * @returns Pageviewview success
 */
const createVisit = async (strapi, payload, visitorUuid) => {
    let { visit = {} } = payload?.data
    let tempVisitId = visit?.uuidv4
    const checkVisitor = await strapi.db.query('api::visitor.visitor').findOne({ where: { uuidv4:visitorUuid }});
    let _visitorId;  
    if (!(checkVisitor && checkVisitor.uuidv4)) {
        let { visitor = {} } = payload?.data
        let visitorObj = createVisitorObject(visitor, visitorUuid);
        const _visiors = await createVisitor(strapi, visitorObj) 
        _visitorId = _visiors?.success?.visitor?.id;
    } else {
        _visitorId = checkVisitor.id;
    }
    let isExist = await strapi.db.query('api::visit.visit').findOne({ where: { uuidv4: tempVisitId }});
    let _visit = {}, _visitId;
    if (!(isExist && isExist.uuidv4)) {
        let visitObj = createVisitObject(visit, tempVisitId, _visitorId)
        _visit = await strapi.query("api::visit.visit").create(visitObj);
        if (_visit?.id) {
            _visitId = _visit?.id;
        }
    } else if (isExist && isExist.uuidv4) {
        _visit = isExist;
        _visitId = isExist.id;
    }
    return { _visit, _visitId };
}

/**
 * INFO: Create pageview
 * @param {Scope} strapi 
 * @param {Object} Pageviewviews
 * @param {String} visit 
 * @returns Pageviewview success
 */
const createPageview = async (strapi, Pageviewviews, pageViewTotal, ip) => {
    if ( pageViewTotal && pageViewTotal > process.env.LOG_IP_ON_PAGEVIEWS_MAX ) {
        Pageviewviews.data.remoteHost = ip
    }
    let pageview = await strapi.query("api::pageview.pageview").create(Pageviewviews);
    return pageview
}

/**
 * INFO: Create visitor
 * @param {Scope} strapi 
 * @param {Object} Pageviewviews
 * @param {String} visit 
 * @returns visitor create success
 */
const createVisitor = async (strapi, createVisitorObject) => {
    let visitor = await strapi.query("api::visitor.visitor").create(createVisitorObject);
    return {
        success: {
            visitor: visitor
        }
    }
}
