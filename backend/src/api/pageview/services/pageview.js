'use strict';

/**
 * pageview service.
 */

const { createCoreService } = require('@strapi/strapi').factories;

module.exports = createCoreService('api::pageview.pageview', ({ strapi }) => ({
    /**
     * INFO: Get pageview by visit id
     * @param {*} filters 
     * @returns Matched pageview
     */
     async pageById(filters) {
        const entries = await strapi.entityService.findMany('api::pageview.pageview', {
            filters: filters,
        });
        return entries
    },
    /**
         * Promise to edit a/an pageview.
         * @return {Promise}
         */
    async edit(pageview, _visitId, _visit, pageViewTotal, ip) {
        let data = {
            visibilityChange: pageview.visibilityChange,
            scrollMax: pageview.scrollMax,
            pageReload: pageview.pageReload,
            duration: pageview.duration,
            linkClicked: pageview.linkClicked,
            applyClicked: pageview.applyClicked,
            emailClicked: pageview.emailClicked,
            contentCopied: pageview.contentCopied,
            applyForm: pageview.applyForm,
            title: pageview.title,
            referrer: pageview.referrer
        }
        if ( pageViewTotal && pageViewTotal > process.env.LOG_IP_ON_PAGEVIEWS_MAX ) {
            data.remoteHost = ip
        }
        return await strapi.query('api::pageview.pageview').update({ where: { uuidv4: pageview.uuidv4, visit: _visitId }, data: data });
    },
}))
