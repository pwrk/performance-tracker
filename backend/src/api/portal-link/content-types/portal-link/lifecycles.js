module.exports = {
  async beforeCreate(data) {},
  async afterCreate(data) {
    /**
     * INFO: Service to send transaction template based mail
     * @param {String} subject
     * @param {String} sendTo
     * @param {String} template
     * @param {Array} mergeTags --> Need to send language based on country or region of reciever user
     */
     const result = strapi.query("api::setting.setting").update({ where: { id: 1 }, data: { lastImport: o.createdAt } });
     console.log("LIFECYCLE After Created:", data.result, result);
  },
};
