'use strict';

async function insert(payload) {
  if (payload.data.url) {
    try {
      // console.log("Link:",  payload.data.job, payload.data.channel, "URL:", payload.data.url.href ? payload.data.url.href : payload.data.url)
      const id = await strapi.query("api::portal-link.portal-link").create(payload);
      console.log("CREATED:", id);
    } catch(e) {
  
    }
  } else {
    // console.log("ignore", payload.data.job, payload.data.channel, );
  }
}
module.exports = { insert }