'use strict';

const mapIdGreenjobs = require('./portals/greenjobs.de.js')
const mapIdKalaydo = require('./portals/kalaydo.de.js')
const mapIdMeinestadt = require('./portals/meinestadt.de.js')
const mapIdJobsintown = require('./portals/jobsintown.de.js')
const mapIdJobvector = require('./portals/jobvector.de.js')
const mapIdJobware = require('./portals/jobware.de.js')
const mapIdJobwareChannels = require('./portals/jobware-channels.js')
const mapIdMonster = require('./portals/monster.de.js')
const mapIdStellenanzeigen = require('./portals/stellenanzeigen.de.js')
const mapIdXing = require('./portals/xing.de.js')
const mapIdYourfirm = require('./portals/yourfirm.de.js')

const portals = {
  'www.greenjobs.de': {
    channel: [ 1497 ],
    template: 'https://www.greenjobs.de/angebote/index.html?id={{ID}}&anz=html',
    callback: (obj) => mapIdGreenjobs(obj),
  },
  'www.kalaydo.de': {
    channel: [ 17 ],
    callback: (obj) => mapIdKalaydo(obj),
  },
  'www.meinestadt.de': {
    channel: [ 2119, 2121, 2277, 2279, 2281, 2169, 1855, 1857, 2759, 2355, 2765, 2361 ],
    callback: (obj) => mapIdMeinestadt(obj),
  },
  'www.stellenanzeigen.de': {
    channel: [ 10, 559, 1225, 1501,2005, 2255, 2443 ],
    template: 'https://www.stellenanzeigen.de/job/{{TITLE}}-{{ID}}/',
    callback: (obj) => mapIdStellenanzeigen(obj),
  },
  'www.jobsintown.de': {
    channel: [ 1, 385, 447 ],
    template: 'https://www.jobsintown.de/jobsuche/job/?job_id={{ID}}',
    callback: (obj) => mapIdJobsintown(obj),
  },
  'www.jobware.de': {
    channel: [ 66, 443, 1005, 1777, 1991, 2055, 2261, 2625  ],
    template: 'https://www.jobware.de/job/{{ID}}',
    callback: (obj) => mapIdJobware(obj)
  },
  'jobs.jobware.de': {
    channel: [ 66, 443, 1005, 1777, 1991, 2055, 2261, 2625  ],
    template: 'https://www.jobware.de/job/{{ID}}',
    callback: (obj) => mapIdJobware(obj)
  },
  'www.jobvector.de': {
    channel: [ 38  ],
    callback: (obj) => mapIdJobvector(obj)
  },
  'www.handwerkerstellen.de': {
    channel: [ 16, 19, 43, 48, 53, 82, 215, 225, 279, 465, 903, 2578 ],
    template: 'https://{{HOST}}/job/{{TITLE}}.{{ID}}.html',
    callback: (obj) => mapIdJobwareChannels(obj)
  },
  'www.monster.de': {
    channel: [ 9, 35, 86, 198, 214, 541, 1947, 2037, 2257, 2329, 2868 ],
    template: 'https://www.monster.com/job-openings/{{TITLE}}--{{ID}}',
    callback: (obj) => mapIdMonster(obj)
  },
  'www.xing.com': {
    channel: [ 56, 445, 933, 1705, 2133, 2049, 2051, 2137, 2153, 2155, 2273, 2287, 2293, 2331, 2381 ],
    callback: (obj) => mapIdXing(obj),
  },  
  'www.yourfirm.de': {
    channel: [ 1271 ],
    callback: (obj) => mapIdYourfirm(obj)
  }
};

const channel = {}
Object.keys(portals).forEach(function(portal,obj) {
    for (const element of portals[portal].channel) {
        channel[element] = portal;
    }
});

module.exports = { portals, channel }