'use strict';

const { insert } = require('../insert.js')

module.exports = function mapIdMonster(o) {
  const payload = {
    data: {
      channel: o.channel,
      job: o.job,
      url: o.referrer?.pathname.startsWith('/stellenangebot/') ? `${o.referrer.origin}${o.referrer.pathname}` : undefined
    }
  }
  insert(payload);
}

