'use strict';

const { insert } = require('../insert.js')

module.exports = function mapIdJobwareChannels(o) {
  const payload = {
    data: {
      channel: o.channel,
      job: o.job,
      url: o.referrer ?? o.url
    }
  }

  if (o.channel == "16" & o.url.host == "jobs.heise.de") {
    insert(payload);
  }
  if (o.channel == "19" & o.url.host == "jobs.ingenieur.de") {
    insert(payload);
  }
  if (o.channel == "53" & o.url.host == "stellenmarkt.wuv.de") {
    insert(payload);
  }
  if (o.channel == "82" & o.url.host == "www.sekretaerin.de") {
    insert(payload);
  }
  if (o.channel == "225" & o.url.host == "www.handwerkerstellen.de") {
    insert(payload);
  }
  if (o.channel == "465" & o.url.host == "jobs.badische-zeitung.de") {
    insert(payload);
  }
  if (o.channel == "903" & o.url.host == "www.stellenmarkt.haufe.de") {
    insert(payload);
  }
}
