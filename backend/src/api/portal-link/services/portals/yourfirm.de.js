'use strict';

const { insert } = require('../insert.js')

module.exports = function mapIdYourfirm(o) {
  const payload = {
    data: {
      channel: o.channel,
      job: o.job,
      url: o.url
    }
  }  
  insert(payload);
}
 
