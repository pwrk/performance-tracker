'use strict';

const { insert } = require('../insert.js')

module.exports = function mapIdJobware(o) {
  
  const id = o.url?.href.match(/\/[0-9]+\/([0-9]+)\/index\.html/) ?? undefined
  
  const payload = {
    data: {
      channel: o.channel,
      job: o.job,
      url: o.portal?.template.replace('{{ID}}',id[1])?.split('?')[0] ?? undefined
    }
  }
  if ( payload.data.url ) {
    insert(payload);
  } else {

  }
}

/**
function getPortalLinkFromReferrer(referrer) {
  var url = undefined;

  if (!referrer) {
    return undefined
  }
  
  switch (referrer?.host) {
    case 'jobs.jobware.net':
      if (referrer.pathname.startsWith('/Job/')) {
        url = `${referrer.origin}${referrer.pathname}`
      }
      break;
    case 'www.jobware.de':
      if (referrer.pathname.startsWith('/job/')) {
        url = `${referrer.origin}${referrer.pathname}`
      }
      break;
  }
  return url
}
**/