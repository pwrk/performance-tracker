'use strict';

const { insert } = require('../insert.js')

module.exports  = function mapIdKalaydo(o) {
  const payload = {
    data: {
      channel: o.channel,
      job: o.job,
      url: o.url?.host == 'www.kalaydo.de' ? `${o.url.origin}${o.url.pathname}` : undefined
    }
  }
  insert(payload);
}

