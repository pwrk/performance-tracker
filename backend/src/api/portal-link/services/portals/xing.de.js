'use strict';

const { insert } = require('../insert.js')

module.exports  = function mapIdXing(o) {
  const payload = {
    data: {
      channel: o.channel,
      job: o.job,
      url: o.url?.host == 'www.xing.com' ? o.url.href : undefined
    }
  }
  insert(payload);
}

