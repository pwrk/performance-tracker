'use strict';

const { insert } = require('../insert.js')

module.exports  = function mapIdMeinestadt(o) {
  const id = o.referrer?.searchParams?.get('id') ?? undefined;
  const url = o.referrer?.host === 'jobs.meinestadt.de' ?? undefined;
  const payload = {
    data: {
      channel: o.channel,
      job: o.job,
      url: (id & url & o.referrer?.host == 'jobs.meinestadt.de') ? `${o.referrer.origin}${o.referrer.pathname}?id=${id}` : undefined
    }
  }
  insert(payload);
}

