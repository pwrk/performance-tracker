'use strict';

const { insert } = require('../insert.js')

module.exports = function mapIdStellenanzeigen(o) {
  const id = o.url.searchParams?.get('autowert') ?? undefined;
  const title = o.title ? normalizeTitle(o.title) : 'Anzeigenmtitel';
  const template = o.portal?.template ?? undefined
  const payload = {
    data: {
      channel: o.channel,
      job: o.job,
      url: template ? template.replace('{{ID}}',id).replace('{{TITLE}}',title)?.split('?')[0] : undefined
    }
  }

  if (o.portal?.channel.includes(1 * o.channel)) {
    insert(payload);
  }
}

function normalizeTitle(title) {
  const normalizedTitle = title.toLowerCase().
                                replace(/ä/g,'ae').
                                replace(/ö/g,'oe').
                                replace(/ü/g,'ue').
                                replace(/ß/g,'ss').
                                replace(/[^a-zA-Z0-9]/g, ' ').replace(/\s+/g, '-');
  return normalizedTitle;
}
