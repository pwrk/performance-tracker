'use strict';

const { insert } = require('../insert.js')

module.exports = function mapIdJobsintown(o) {
  const payload = {
    data: {
      channel: o.channel,
      job: o.job,
      url: o.portal?.template.replace('{{ID}}',o.job) ?? o.url
    }
  }
  insert(payload);  
}

