'use strict';

const { insert } = require('../insert.js')

module.exports = function mapIdVector(o) {
  const payload = {
    data: {
      channel: o.channel,
      job: o.job,
      url: o.url.host == 'www.jobvector.de' ? o.url.href : undefined
    }
  }
  if ( payload.data.url ) {
    console.log("JOBVECTOR", payload);
    insert(payload);
  }
}

