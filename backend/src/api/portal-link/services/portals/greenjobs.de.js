'use strict';

const { insert } = require('../insert.js')

module.exports = function mapIdGreenjobs(o) {

  const id = o.referrer?.href?.match(/www.greenjobs.de\/s\/manager\/angebote\/index.html\?id=([0-9]+)&/) ?? undefined

  if (id) {
    const payload = {
      data: {
        channel: o.channel,
        job: o.job,
        url: o.portal?.template.replace('{{ID}}',id[1]) ?? undefined
      }
    }
    insert(payload);  
    console.log("GREENJOBS", id, payload.data, o.portal, o.url)
  }
}

