'use strict';

/**
 * portal-link service
 */

const { portals, channel } = require('./portals.js');
const { insert } = require('./insert.js')
const { createCoreService } = require('@strapi/strapi').factories;

module.exports = createCoreService('api::portal-link.portal-link', ({ strapi }) => ({
    /**
     * INFO: import 
     */
    async importPortalLinks() {
        try {
            const date = await lastImportDate();
            console.log('date:', date);
            const client = strapi.db.connection.context.client.config.client;
            const startDate = (client == 'better-sqlite3') ? new Date(Number(date)) : new Date(date);
            const endDate =  (client == 'better-sqlite3') ? new Date(Number(date)) : new Date(date);
            endDate.setDate(endDate.getDate() + 5);
    
            const start = startDate.toISOString().split('.')[0].replace('T',' ')
            const end = endDate.toISOString().split('.')[0].replace('T',' ')
    
            const sql = (client == 'better-sqlite3') ? 
                `select pageview.created_at, pageview.job, pageview.referrer, uri, pageview.title, pageview.channel ` +
                `from pageview left join portal_links on (pageview.job = portal_links.job and pageview.channel = portal_links.channel) ` +
                `where portal_links.job IS NULL and pageview.updated_at > date('${start}') and ` + 
                `pageview.updated_at < date('${end}') limit 1500` :
      
                `select pageview.created_at, pageview.job, pageview.referrer, uri, pageview.title, pageview.channel ` + 
                `from pageview left join portal_links on (pageview.job = portal_links.job and pageview.channel = portal_links.channel) ` + 
                `where isnull(portal_links.job) and pageview.created_at > "${start}" and ` + 
                `pageview.created_at < "${end}" order by pageview.created_at asc limit 3000`;
      
            const result = await strapi.db.connection.raw(sql).then((row) => row);
    
            const data = (client == 'better-sqlite3') ? result : result[0];

            return data;
        } catch (error) {
             console.error(error);
        }
    },
    async getExternalPortalLink(row) {
        var url = ""
        try {
          url = new URL(row.uri);
        } catch (e) {
          url = "http://example.de"
        }
        const urlReferrer = row.referrer != null ? new URL(row.referrer) : undefined;
        const title = row.title ?? 'Anzeigenmtitel';
        var result;
        
        const payload = {
            url: url,
            referrer: urlReferrer,
            title: title,
            channel: row.channel,
            job: row.job,
            createdAt: row.created_at,
            portal: portals[url.host] ?? portals[urlReferrer?.host] ?? undefined
        }
        
        

        if (portals[channel[row.channel]]) {
            portals[channel[row.channel]].callback(payload);
        } else {
            mapIdDefault(payload);
        }
        return row.created_at;
    }

}));



function mapIdDefault(o) {
  const payload = {
    data: {
      channel: o.channel,
      job: o.job,
      url: o.url
    }
  } 
  insert(payload);
}

/**
 * INFO: Get min(pageviews.created_at). 
 * Only needed to generate initial query
 */
async function minPageviewDate() {
      const sql = `select min(created_at) as date from pageview`; 
      const client = strapi.db.connection.context.client.config.client;
      try {
        let result = await strapi.db.connection.raw(sql).then((row) => row);    
        const date = (client == 'better-sqlite3') ? result[0].date : result[0][0].date
        return new Date(date);
      } catch (error) {
        console.error(error);
      }
}
    
/*
 * INFO: get latest import date.
 */
async function lastImportDate() {
      const date = await strapi.query("api::setting.setting").findOne({where: { id: 1 }});
      return (date === null || date.lastImport  === null) ? minPageviewDate() : new Date(date.lastImport);
}