'use strict';
var keys = [];

//import { portals, channel } from './config.js'
const { portals, channel } = require("./config")
const { insert } = require('./insert.js')

function mapIdVector(o) {
  const payload = {
    data: {
      channel: o.channel,
      job: o.job,
      url: o.url.host == 'www.jobvector.de' ? o.url.href : undefined
    }
  }
  if ( payload.data.url ) {
    console.log("JOBVECTOR", payload);
    insert(payload);
  }
}

function mapIdDefault(o) {
  const payload = {
    data: {
      channel: o.channel,
      job: o.job,
      url: o.url
    }
  } 
  insert(payload);
}

module.exports = { portals, channel }