'use strict';

/**
 * portal-link controller
 */

const { createCoreController } = require('@strapi/strapi').factories;

module.exports = createCoreController('api::portal-link.portal-link');
