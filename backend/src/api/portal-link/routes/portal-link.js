'use strict';

/**
 * portal-link router
 */

const { createCoreRouter } = require('@strapi/strapi').factories;

module.exports = createCoreRouter('api::portal-link.portal-link');
