'use strict';

module.exports = {
  /**
   * An asynchronous register function that runs before
   * your application is initialized.
   *
   * This gives you an opportunity to extend code.
   */
  register(/*{ strapi }*/) {},

  /**
   * An asynchronous bootstrap function that runs before
   * your application gets started.
   *
   * This gives you an opportunity to set up your data model,
   * run jobs, or perform some special logic.
   */
  bootstrap({ strapi }) {
    const knex = strapi.db.connection;
    const client = knex.context.client.config.client
    if (client == 'mysql') {
      console.debug('check for custom indexes');
      let indexes = {
        pageview: {
          'job':1, 
          'created_at':1, 
          'updated_at':1
        },
        portal_links: {
          'job': 1
        }
      };
      for (const [table, index] of Object.entries(indexes)) {
        for (const indexname in index) {
          createIndexIfNotExist(knex, indexname, table);
        }
      }
    }
  }   
};


async function createIndexIfNotExist(knex, index, table) {
  const sql = `SELECT COUNT(1) indexExists from INFORMATION_SCHEMA.STATISTICS WHERE table_schema=DATABASE() AND table_name='${table}' AND index_name='${index}'`; 
  try {
    let result = await knex.raw(sql).then((row) => row);
    if (result[0][0].indexExists == 0) {
        console.log(`Index ${index} in table ${table} does not exist. Create it!`);
        await knex.schema.alterTable(table, async t => {
        table == 'pageview' ? t.index(index, index) : t.unique(['job', 'channel'], index);
      })
    }
    return result[0][0].indexExists;
  } catch (error) {
    console.error(error);
  }
}
