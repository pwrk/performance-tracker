const GITLAB_TOKEN = process.env.GITLAB_TOKEN ? process.env.GITLAB_TOKEN.trim() : '';
// Your repository
const REPO = 'https://bms:' + GITLAB_TOKEN + '@gitlab.com/pwrk/performance-tracker.git';

module.exports = {
  apps : [{
    name: '@pwrk/performance-tracker',
    script: 'yarn',
    args: 'strapi start',
    interpreter: '/bin/bash',
    env: {
      NODE_ENV: "development",
      PORT: 1337
    },
    env_production: {
      NODE_ENV: "production",
      PORT: 3002
    }
  }],
  
  deploy : {
    production: {
      user : 'yawik',
      host : 'pm2.jobsintown.de',
      ref  : 'origin/main',
      repo : REPO,
      path : '/home/yawik/pt.jobsintown.de',
      'pre-deploy-local' : 'rsync -a --delete build/ yawik@pm2.jobsintown.de:pt.jobsintown.de/source/backend/build/',
      'post-deploy' : 'cd /home/yawik/pt.jobsintown.de/source/backend/ && pm2 startOrRestart ecosystem.config.js --interpreter bash --env production'
    },
    development: {
      user : 'yawik',
      host : 'localhost',
      ref  : 'origin/main',
      repo : REPO,
      path : '/home/strapi/bms.jobsintown.de',
      'pre-deploy-local' : 'rsync -a --delete /home/strapi/backend/build/ /home/strapi/pm2/build/',
      'post-deploy' : 'pm2 startOrRestart ecosystem.config.js --interpreter bash --env development'
    }    
  }
};
