const fs = require('fs');
const { setupStrapi, cleanupStrapi } = require("./helpers/strapi");

jest.setTimeout(10000)

beforeAll(async () => {
  await setupStrapi();
});

afterAll(async () => {
  await new Promise(resolve => setTimeout(() => resolve(), 500));
  await cleanupStrapi();
});

it("strapi is defined", () => {
  expect(strapi).toBeDefined();
});

require('./hello');
