const request = require('supertest');

it("send pixel tracking, expect image", async () => {
  await request(strapi.server.httpServer)
    .get("/api/pageview/908611/1497/59692/Qualit%C3%A4tsicherungs-Auditor%20%28m%2Fw%2Fd%29%20f%C3%BCr%20GLP")
    .expect(200) // Expect response http code 200
    .then((data) => {
      expect(data.res.rawHeaders).toEqual(expect.arrayContaining(['Content-Type', 'image/gif']));
    });
});


it("send unauthenticated request", async () => {
  await request(strapi.server.httpServer)
    .get("/api/pageviews/908611")
    .expect(403) // Expect response http code 200
});