# Performance Tracker

The Performance Tracker records key data to measure the success of job advertisements. Visitors, visits and page views are recorded. The data is collected with Javascript. The data is transmitted to a backend via sendBeacon.

This repository contains the tracking code and a backend and two test jobs

## Requirements

node 18

## Status

WIP (work in progess)

----

You can publish job ads via different channels. For example, via Google Jobs, Facebook Jobs, a special job portal or your own homepage.

## The following questions should be answered:

- How many different people have seen a job ad via the one specific channel.
- How long was an ad viewed on average?
- How long was an ad viewed on average in channel XY?
- How many times was a job ad viewed by one person?
- How often was a job ad viewed by one person in channel XY?
- What is the average loading time of a job ad?
- What is the average loading time of a job ad in Channel XY?

## Visitor:

We have to respect data privacy and avoid to infer a natural person by tracking. We therefore define a visitor as an end device (browser, cell phone, tablet). For a Visitor, a unique key is set in the LocalStorage.

## Visit:

A visit has a temporal end. After [MAX_VISIT_TIME](https://gitlab.com/pwrk/performance-tracker/-/blob/main/job/assets/tracker.js#L4), a visit and all associated pageviews are deleted from LocalStorage.

## Pageview:

Unlike tracking with a tracking pixel (Google Analytics, Matomo, etc), the transmission of data (the request) does not take place at the beginning (after the onload event), but at the end ([onVisibilityChange](https://developer.mozilla.org/en-US/docs/Web/API/Document/visibilitychange_event)=hidden). The tracking code creates unique IDs for a visitor, a visit and a pageview.

The end of a pageview is sent when:

- the tab in the browser goes to the background.
- when the browser is closed and the tab was visible.
- when the operating system goes to sleep.

The tracking code defines visitor, visit and pageview. 

# Installation

```
git clone https://gitlab.com/pwrk/performance-tracker.git
cd performance-tracker
yarn && yarn bootstrap
cp backend/.env.example backend/.env
cp packages/tracker/.env.example packages/tracker/.env
SEND_BEACON_TARGET='http://127.0.0.1:1337' yarn build
cd backend 
yarn && yarn develop
```

# Tests 

If you are on localhost, you should use 127.0.0.1 instead of localhost. 
e2e test can be started by.

```
bash run_tests.sh
```

# Testjobs

https://pwrk.gitlab.io/performance-tracker/

If you start a webserver in the project root (e.g. [caddy file-server](https://caddyserver.com/docs/caddyfile/directives/file_server)) you can access these test jobs at http://localhost/job

# Backend 

https://pt.jobsintown.de


# Usecases

https://gitlab.com/pwrk/performance-tracker/-/wikis/usecases

