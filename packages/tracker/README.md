# Tracker

the tracker allows to record the number of visitors, visits and page views of job advertisements. The tracker creates unique identifiers in the user's LocalStorage. Captured data is sent to a backend via sendBeacon API.

A visitor gets an identifier with no expiration date. All page views with known identifier are credited to a visitor. If the tag is deleted from the LocalStorage, the tracker creates a new tag on the next pass.

The identifier for a visit has an expiration date. After 30 minutes of inactivity, a visit is considered over. Example: a user goes on his lunch break for an hour and returns to his browser. Follow-up actions in the browser are then considered a new visit.

Pageview: Each page that is tracked contains an identifier of the page and an identifier of the channel.

# Status

development

## Pageview

| Name                    | Description                                                   |
|-------------------------|---------------------------------------------------------------|
| linkClicked             | latest link clicked                                           |
| emailClicked            | latest email clicked                                          |
| applyClicked            | application link clicked                                      |
| contentCopied           | count the content has been copied                             |
| duration                | accumulated time of visibility                                |
| firstLoadingTime        | Time from the start of the tracker to the onLoad event        |
| viewHeight              | height of the viewport                                        | 
| viewWidth               | width of the viewport                                         |
| mouseTrail              | mouse mouvements                                              |
| scrollMax               | scroll max 1.0 means user scrolled to the bottom of the page  |
| remoteHost              | IP of the user sending data                                   |

## .env

you have to set the following values as evironment var. 

| Name                    | Description                                                   |
|-------------------------|---------------------------------------------------------------|
| SEND_BEACON_TARGET      | Destination to which the data is sent via sendbeacon.         |
| MAX_VISIT_LENGTH        | Maximum time of inactivity in ms before a new session starts  |
| CLASS_APPLICATION_LINK  | Identifier for Application Links.                             |

## Disable Tacking

it' possible to disable the execution of the tracking code by adding a GET
parameter `disable-tracking=1`

## Development


During development, you can use 2 example job advertisements. There are 2 html pages that have the tracking code embedded. If the hostname is not "pwrk.gitlab.io", then load the tracking code from the
`dist/` directory. 

After a change to `src/tracker.js` the version can be updated via `yarn build` to update the version.


