/**
 * you have to set SEND_BEACON_TARGET and MAX_VISIT_LENGTH as an environment
 * variable to be able to build this script via:
 * 
 * yarn build
 */
import { SEND_BEACON_TARGET, MAX_VISIT_LENGTH, CLASS_APPLICATION_LINK, DISABLE_TRACKING } from 'env'

const PAGEVIEW = 'pageview';
const VISIT = 'visit';
const VISITOR = 'visitor';

function uuidv4(val) {
  let data;
  const uuidv4 = ([1e7]+-1e3+-4e3+-8e3+-1e11).replace(/[018]/g, c =>
    (c ^ crypto.getRandomValues(new Uint8Array(1))[0] & 15 >> c / 4).toString(16)
  );
  if (val === VISIT) {
    data = JSON.stringify({
      uuidv4: uuidv4, 
      start: Date.now(),
      channel: _pt.CHANNEL,
      pageviews: 1,
      referrer: document.referrer
    });
  } else if (val === PAGEVIEW) {
    data = JSON.stringify({
      firstLoadingTime: 0,
      uuidv4: uuidv4,
      job: _pt.PAGE,
      channel: _pt.CHANNEL,
      visibilityChange: 0,
      contentCopied: 0,
      scrollMax: 0,
      duration: 0,
      pageReload: 0,
      viewHeight: window.innerHeight,
      viewWidth: window.innerWidth,
      uri: window.location.href,
      title: window.document.title,
      referrer: document.referrer
    });
  } else {
    data = JSON.stringify({
      uuidv4: uuidv4,
      agent: window.navigator.userAgent
    });
  }
  return(data);
}


/**
 * 
 * @returns 
 */
function visitor() {
  const data = JSON.parse(localStorage.getItem(VISITOR) ?? uuidv4(VISITOR));
  data.screenHeight = window.screen.height;
  data.screenWidth = window.screen.width;
  data.languages = navigator.languages;
  data.language = navigator.language;
  data.timezone = Intl.DateTimeFormat().resolvedOptions().timeZone;
  
  console.log("Visitor", data);
  localStorage.setItem(VISITOR, JSON.stringify(data));
  return data;
}
  
function visit() {
  const data = JSON.parse(localStorage.getItem(VISIT) ?? uuidv4(VISIT));
  if (data.start === undefined) {
    data.start = Date.now();
    data.referrer = window.location.href;
  }
  console.log(
    "Visit: ",
    data.uuidv4,
    " started at:",
    data.start,
    ". updated at: ",
    data.update
  );
  localStorage.setItem(VISIT, JSON.stringify(data));
  return data;
}

function updateVisitTime() {
  const visit = localStorage.getItem(VISIT);
  var data;
  if (visit) {
    console.log("VISIT from localstorage", visit);
    data = JSON.parse(visit);
  }
  if (!data) {
    console.log("data from pt context");
    data = window.__pt_context_obj.data.visit;
  }
  const now = Date.now();
  console.log(data, now, data.update);
  if (now - data.update > MAX_VISIT_LENGTH) {
    var iterator;
    Object.keys(localStorage).find((el) => {
      if (el === "visit" || el.substring(0, 9) === "pageview-") {
        console.log("Expire");
        localStorage.removeItem(el);
      }
    });
    data = window.__pt_context_obj;
  } else {
    data.update = now;
    data.pageviews ? data.pageviews++ : 1;
  }
  localStorage.setItem(VISIT, JSON.stringify(data));
  return data;
}

function pageview(page, channel) {
  const name = PAGEVIEW + "-" + page;
  const data = JSON.parse(localStorage.getItem(name) ?? uuidv4(PAGEVIEW));

  data.start = Date.now();
  if (data.pageReload === undefined) {
    data.visibilityChange = 0;
    data.firstLoadingTime = 0;
    data.contentCopied = 0;
    data.job = page;
    data.channel = channel;
    data.pageReload = 0;
    data.duration = 0;
  } else {
    data.pageReload++;
  }
  console.log("Pageview", data, page, channel);
  localStorage.setItem(name, JSON.stringify(data));
  return data;
}

// must be run after the pageview is created
function saveJobCookie() {
  const job_id = window.__pt_context_obj.data.pageview.job;
  if (job_id) {
    document.cookie = `job_id=${job_id};SameSite=None; path=/;`;
  }
}

function getJobCookie() {
  const name = 'job_id';
  let cookieArr = document.cookie.split(";");
  for(let i = 0; i < cookieArr.length; i++) {
    let cookiePair = cookieArr[i].split("=");
    if(name == cookiePair[0].trim()) {
      return decodeURIComponent(cookiePair[1]);
    }
  }
  return null;
}

function appendJobIdToApplicationForm() {
  const jobId = getJobCookie();
  if (jobId) {
    const forms = document.getElementsByTagName("form") || [];
    Array.from(forms).forEach(form => {
      if (form.id === "applicationForm") {
        const input = document.createElement("input");
        input.type = "hidden";
        input.id = "job_id";
        input.name = "job_id";
        input.value = jobId;
        form.appendChild(input);
      }
    });
  }
}

function init(body) {

  saveJobCookie();
  
  window.addEventListener("scroll", () => {
    let scrollTop = window.scrollY || window.pageYOffset;
    let reached = scrollTop + window.innerHeight;
    let docHeight = document.documentElement.scrollHeight || 1;
    let scrollPercent = reached / docHeight;
    let scrollPercentRounded = Math.round(scrollPercent * 10);
    if (scrollPercentRounded > body.scroll_max) {
      body.scroll_max = scrollPercentRounded;
    }
  });

  document.addEventListener("copy", function() {
    body.data.pageview.contentCopied += 1;
  })

  document.addEventListener("click", function(event) {
    if (event.target.nodeName === "A") {
      let href = event.target.href;

      if (href.startsWith("mailto:")) {
        body.data.pageview.emailClicked = href.slice(7);
      } else {
        body.data.pageview.linkClicked = event.target.href;
      }
    }
    if (
      event.target.classList.contains(CLASS_APPLICATION_LINK) ||
      event.target.closest('button[data-testid="harmonised-apply-button"]')
    ) {
      body.data.pageview.applyClicked = true;
    }
  });

  document.addEventListener("submit", function (event) {
    if ( _pt.CONTEXT === "applicationForm" && event.target.id === "applicationForm") {
      body.data.pageview.applyForm = true;
    }
  });

  document.onvisibilitychange = function () {
    if (document.visibilityState === "visible") {
      updateVisitTime();
    }

    if (document.visibilityState === "hidden") {
      body.data.pageview.duration += Date.now() - body.data.pageview.start || 1;
      body.data.pageview.visibilityChange++ || 1;

      body.data.pageview.scrollMax = Math.round(body.scroll_max) / 10;

      let visit = localStorage.getItem(VISIT);
      if (visit != null) {
        body.data.visit = JSON.parse(visit);
      }

      body.data.end = Date.now();
      navigator.sendBeacon(
        SEND_BEACON_TARGET + "api/pageviews",
        JSON.stringify(body)
      );
    } else {
      body.data.pageview.start = Date.now();
      body.data.start = Date.now();
      console.log("VISIBLE", document.visibilityState);
    }

    localStorage.setItem(
      PAGEVIEW + "-" + _pt.PAGE,
      JSON.stringify(body.data.pageview)
    );
  };

  window.addEventListener("load", function (event) {
    const timeToLoad = Date.now() - body.data.start;
    body.data.pageview.firstLoadingTime = timeToLoad;
    body.data.start = Date.now();
    localStorage.setItem(
      PAGEVIEW + "-" + _pt.PAGE,
      JSON.stringify(body.data.pageview)
    );
    updateVisitTime();
  });

  appendJobIdToApplicationForm();

}
  
const params = new URL(window.location.href).searchParams;
if(params.get(DISABLE_TRACKING)) {
} else {
  window.__pt_context_obj = { data :
    {
      start: Date.now(),
      visitor: visitor(),
      visit: visit(),
      pageview: pageview(_pt.PAGE, _pt.CHANNEL)
    },
    scroll_max : 0
  };
  init(window.__pt_context_obj);
}
