#!/usr/bin/env node

require('dotenv').config();

const clientEnv = process.env.NODE_ENV ?? 'development';

const config = {
  logLevel: "info",
  entryPoints: ["src/tracker.js"],
  bundle: true,
  minify: clientEnv === 'production',
  outfile:  clientEnv === 'production' ? "dist/tracker.min.js" : "dist/tracker.js",                      
}

let envPlugin = {
  name: 'env',
  setup(build) {
    // Intercept import paths called "env" so esbuild doesn't attempt
    // to map them to a file system location. Tag them with the "env-ns"
    // namespace to reserve them for this plugin.
    build.onResolve({ filter: /^env$/ }, args => ({
      path: args.path,
      namespace: 'env-ns',
    }))
    // Load paths tagged with the "env-ns" namespace and behave as if
    // they point to a JSON file containing the environment variables.
    build.onLoad({ filter: /.*/, namespace: 'env-ns' }, () => ({
      contents: JSON.stringify(process.env),
      loader: 'json',
    }))
  },
}

config['plugins'] = [envPlugin];

if (clientEnv === 'production') {
  config['minify'] = true;
  config['outfile'] = 'dist/tracker.min.js';
  config['drop'] = ['debugger', 'console']
} else {
  config['minify'] = false;
  config['outfile'] = 'dist/tracker.js';  
} 

require("esbuild")
  .build(config)
  .catch(() => process.exit(1));
  
