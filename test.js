const jsdom = require("jsdom");
const { JSDOM } = jsdom;

const waitOn = require('wait-on');

const opts = {
  resources: [
    'tcp:127.0.0.1:1337',
  ],
  delay: 1000, // initial delay in ms, default 0
  interval: 100, // poll interval in ms, default 250ms
  simultaneous: 1, // limit to 1 connection per resource at a time
  timeout: 600000, // timeout in ms, default Infinity
  tcpTimeout: 600000, // tcp timeout in ms, default 300ms
  window: 1000 // stabilization time in ms, default 750ms
};

const save_delay = 250;

const axios = require('axios').default;

async function enable_public_pageview() {

    let response = await axios.post('http://127.0.0.1:1337/admin/login', {
        email: 'test@test.test',
        password: 'Testing1234',
    });

    let token = response.data.data.token;

    response = await axios.get('http://127.0.0.1:1337/users-permissions/roles/2', {
        headers: {
            Authorization: 'Bearer ' + token,
        },
    });

    let perms = response.data.role.permissions;
    perms['api::pageview'].controllers.pageview.create.enabled = true;
    perms['api::pageview'].controllers.pageview.find.enabled = true;
    perms['api::pageview'].controllers.pageview.findOne.enabled = true;

    await axios.put('http://127.0.0.1:1337/users-permissions/roles/2', {
        permissions: perms }, {
            headers: {
                Authorization: 'Bearer ' + token,
            },
    });
}

async function get_pageviews(url) {
  const response = await axios.get('http://127.0.0.1:1337/api/pageviews');
  return response.data.data.filter(view => view.attributes["uri"] == url)
}

async function get_one_pageview(url) {
  let pageviews = await get_pageviews(url);
  return pageviews[0];
}

// bconst assert = require('node:assert').strict;


function assert(condition, message) {
    if (!condition) {
        throw new Error(message || "Assertion failed");
    }
}

async function sched_test(url, test, storage=null) {
  const virtualConsole = new jsdom.VirtualConsole();
  virtualConsole.sendTo(console);
  virtualConsole.on("jsdomError", err => { throw err; });

  const beforeParse = (window) => {
    if (storage) {
      storage = JSON.parse(storage);
      Object.keys(storage).forEach(k => {
        window.localStorage.setItem(k, storage[k]);
      });
    }
  }

  let opt = { resources: 'usable',
              runScripts: 'dangerously',
              virtualConsole,
              beforeParse,
              referrer: 'https://test.de' };
  let dom = await JSDOM.fromURL(url, opt);

  // XXX: jsdom is missing navigator.sendBeacon() ...
  let script = dom.window.document.createElement("script");
  script.type = "text/javascript";
  script.src = "https://unpkg.com/navigator.sendbeacon";
  dom.window.document.head.appendChild(script);

  // We want to be the last 'load' event..
  dom.window.addEventListener('DOMContentLoaded', event => {
    dom.window.addEventListener('load', event => test(dom, event));
  });

  Object.defineProperty(dom.window.document.documentElement,
                        'scrollWidth', {
                        configurable: true,
                        value: 1000
  });
  Object.defineProperty(dom.window.document.documentElement,
                        'scrollHeight', {
                        configurable: true,
                        value: 1000
  });

  dom.window.scroll = function(x, y) {
    let maxWidth = Math.max(0, dom.window.document.documentElement.scrollWidth - dom.window.innerWidth);
    let maxHeight = Math.max(0, dom.window.document.documentElement.scrollHeight - dom.window.innerHeight);
    dom.window.scrollX = dom.window.pageXOffset = Math.min(x, maxWidth);
    dom.window.scrollY = dom.window.pageYOffset = Math.min(y, maxHeight);
    dom.window.dispatchEvent(new dom.window.CustomEvent('scroll'));
  }
}

// XXX: jsdom has no layouting so we trigger the visibility event manually
function set_visible(dom, visible) {
    let val = visible ? 'visible' : 'hidden';

    if (visible && dom.window.document.visibilityState === 'visible')
        return;
    if (!visible && dom.window.document.visibilityState === 'hidden')
        return;

    Object.defineProperty(dom.window.document, "visibilityState", {
                        configurable: true,
                        get: function() { return val; }
    });
    dom.window.document.onvisibilitychange();
}

const run_generic_tests = async (url) => {
  await sched_test(url, async (dom, _) => {
    assert(dom.window.document.onvisibilitychange !== undefined);

    let pageviews = await get_pageviews(url);
    assert(pageviews.length === 0);

    set_visible(dom, false);
    await new Promise(resolve => setTimeout(resolve, save_delay));
    let pageview = await get_one_pageview(url);
    
    console.log("### PAGEVIEW ###", pageview);

    assert(pageview.attributes.remoteHost === null);
    assert(pageview.attributes.scrollMax === 0);
    
    dom.window.scroll(0, 100);

    // click on email with css pwrk-application-link class 
    let button = dom.window.document.getElementById('apply_mail');
    let href = button.getAttribute('href').slice(7);
    button.click();
    set_visible(dom, true);
    set_visible(dom, false);
    await new Promise(resolve => setTimeout(resolve, save_delay));
    pageview = await get_one_pageview(url);

    assert(pageview.attributes.remoteHost === "127.0.0.1");
    assert(pageview.attributes.emailClicked === href);
    assert(pageview.attributes.applyClicked === true);

    assert(pageview.attributes.scrollMax === 0.9);
    dom.window.scroll(0, 300);

    // click in link with css pwrk-application-link class
    button = dom.window.document.getElementById('apply_link');
    href = button.getAttribute('href');
    button.click();
    set_visible(dom, true);
    set_visible(dom, false);
    await new Promise(resolve => setTimeout(resolve, save_delay));
    pageview = await get_one_pageview(url);

    assert(pageview.attributes.remoteHost === '127.0.0.1');
    assert(pageview.attributes.linkClicked === href);
    assert(pageview.attributes.applyClicked === true);
    
    let title = dom.window.document.title;
    let referrer = dom.window.document.referrer;
    assert(pageview.attributes.title === title);
    assert(pageview.attributes.referrer === referrer);
    assert(pageview.attributes.scrollMax === 1);

    await sched_test(url, async (dom, _) => {
      // test that localStorage works
      await get_one_pageview(url);
      set_visible(dom, false);
      await new Promise(resolve => setTimeout(resolve, save_delay));
      await get_one_pageview(url);

      // test that localStorage is not required
      dom.window.localStorage.clear();
      set_visible(dom, true);
      set_visible(dom, false);
      await new Promise(resolve => setTimeout(resolve, save_delay));
      await get_one_pageview(url);

    }, JSON.stringify(dom.window.localStorage));
  });
}

const run_disable_tracking_test = async (url) => {
  await sched_test(url, async (dom, _) => {
    assert(dom.window.document.onvisibilitychange === undefined);
  });
}

const run_dataid_button_test = async (url) => {
  await sched_test(url, async (dom, _) => {
    dom.window.document.querySelector('button[data-testid="harmonised-apply-button"]').click();

    set_visible(dom, false);
    await new Promise(resolve => setTimeout(resolve, save_delay)); // hidding DOM triggers event manually
    const pageview = await get_one_pageview(url);

    assert(pageview.attributes.applyClicked === true);
  })
}

const run_copy_content_test = async (url) => {
  await sched_test(url, async (dom, _) => {
    set_visible(dom, false);
    await new Promise(resolve => setTimeout(resolve, save_delay));

    const pageview1 = await get_one_pageview(url);
    assert(pageview1.attributes.contentCopied === 0);

    const textElement = dom.window.document.getElementById("hello_world")
    const copyEvent = dom.window.document.createEvent('Event')
    copyEvent.initEvent("copy", true, true);

    textElement.dispatchEvent(copyEvent);

    set_visible(dom, true);
    set_visible(dom, false);
    await new Promise(resolve => setTimeout(resolve, save_delay));

    const pageview2 = await get_one_pageview(url);
    assert(pageview2.attributes.contentCopied === 1);
  })
};

const run_form_apply_test = async (url) => {
  await sched_test(url, async (dom, _) => {

    const form = dom.window.document.querySelector('form#applicationForm');

    const event = new dom.window.Event('submit', {
      bubbles: true,
      cancelable: true
    });
    form.dispatchEvent(event); // Manually trigger form submission

    set_visible(dom, false); 
    await new Promise(resolve => setTimeout(resolve, save_delay));
    pageview = await get_one_pageview(url);

    assert(pageview.attributes.applyForm === true); // test that form submission is tracked

  }, JSON.stringify({'CONTEXT': 'applicationForm'})); // set localStorage
}

const run_succ_page_pv_test = async (url) => {
  await sched_test(url, async (dom, _) => {
    let pageviews = await get_pageviews(url);
    assert(pageviews.length === 0);

    set_visible(dom, false);
    await new Promise(resolve => setTimeout(resolve, save_delay));
    let pageview = await get_one_pageview(url);
    assert(pageview.attributes.remoteHost === null);
    assert(pageview.attributes.scrollMax === 0);
    
    set_visible(dom, true);
    dom.window.scroll(0, 300);
    
    set_visible(dom, false);
    await new Promise(resolve => setTimeout(resolve, save_delay));
    pageview = await get_one_pageview(url);

    assert(pageview.attributes.remoteHost === '127.0.0.1');
    
    let title = dom.window.document.title;
    let referrer = dom.window.document.referrer;
    assert(pageview.attributes.title === title);
    assert(pageview.attributes.referrer === referrer); // should check from form, but not const
    assert(pageview.attributes.scrollMax === 1);
  }, JSON.stringify({'CONTEXT': 'applicationFormSuccess'}));
};

const count_applicants = async () => {
  
  // !important 
  // need to schedule this because the pageview's are loaded scheduled also (not async await)
  setTimeout( async () => { 
    
    const applicants = { 
      jobAd: 0, 
      applicationForm: 0, 
      applicationFormSuccess: 0
    };
    
    const response = await axios.get('http://127.0.0.1:1337/api/pageviews');
    response.data.data.forEach((view) => {
      if (view.attributes.applyClicked) {
        applicants.jobAd++;
      }
      if (view.attributes.applyForm) {
        applicants.applicationForm++;
      }
      if (view.attributes.uri === 'http://127.0.0.1:1337/job_succ.html') {
        applicants.applicationFormSuccess++;
      }
    });
    
    console.log("APPLICANTS", applicants);

  }, 5000);
};

(async () => {
    await waitOn(opts);
    await enable_public_pageview();

    await run_generic_tests("http://127.0.0.1:1337/job.html");
    await run_disable_tracking_test("http://127.0.0.1:1337/job.html?disable-tracking=true");
    await run_dataid_button_test("http://127.0.0.1:1337/job.html?data-testid=1");
    await run_copy_content_test("http://127.0.0.1:1337/job.html?copy-content=true");

    await run_form_apply_test("http://127.0.0.1:1337/job_form.html");
    await run_succ_page_pv_test("http://127.0.0.1:1337/job_succ.html");

    await count_applicants();
})();
